# About and Info

**Updates and Information about the club will be posted here. Please turn on notifications for file edits.**

## Official Matrix Space
All members are **required** to install a [Matrix client](https://matrix.org/clients/) and join the Space.
A recommended Matrix client is [Element](https://element.io/get-started). Follow instructions to register an account and install a client for your mobile phone/computer. Information about sessions, tasks etc will be sent through Matrix, if you are not in the Matrix room, you will miss important information.

[Use this link to join the Space](https://matrix.to/#/!OQcMYHVUNjbVBnwnLV:matrix.org?via=matrix.org)

[DIS Digital Design Discord](https://discord.gg/6wBrMgJb)


## Meeting Time and Location

We will have weekly meetings on Fridays in the Secondary Library, from 14:45 to 15:45. 
**If you cannot come, please send a message in the Matrix room beforehand.**

## Software Licencing

Software Licencing basically means how other people are allowed to use your software. At DIS Coding Club, we strongly believe that software freedom is of high importance and we should not restrict anyone from using software. Therefore, all software created by the DIS Coding Club i.e. projects under the DIS Coding Club Gitlab account **must** be licenced under a [GPL Compatible Free Software Licence](https://www.gnu.org/licenses/license-list.en.html#GPLCompatibleLicenses). 
We recommend licences such as the GPLv3, MIT and the 3-clause BSD Licence. Read more about Free Software and Free Software Licences [here](https://www.fsf.org/licensing/). We also recommend using Free Software as much as possible to protect your freedom.

## Essential software you should to install
- [Git](https://git-scm.com/)
- Windows users should install [WSL](https://docs.microsoft.com/en-us/windows/wsl/install) and Ubuntu
- Linux/Mac users make sure to have a terminal emulator
- [Python](https://wiki.python.org/moin/BeginnersGuide/Download)
- A IDE/Text editor, here are some you could use:
    * [Sublime Text](https://www.sublimetext.com/)
    * [VSCodium (VSCode without Microsoft)](https://vscodium.com/)
    * [PyCharm (For Python only)](https://www.jetbrains.com/pycharm/download/)
